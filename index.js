//dependencies
const ViberBot = require('viber-bot').Bot,
BotEvents = require('viber-bot').Events,
TextMessage = require('viber-bot').Message.Text,
RichMedia = require('viber-bot').Message.RichMedia,
ImageMessage = require('viber-bot').Message.Picture,
KeyboardMessage = require('viber-bot').Message.Keyboard,
express = require('express');
const app = express();


process.env.BOT_ACCOUNT_TOKEN="4d34558606e7d21b-c81cba819aef5e7-4542b95e7962f504"
process.env.EXPOSE_URL='https://570233fc58c6.ngrok.io'

//check for env variables
if (!process.env.BOT_ACCOUNT_TOKEN) {
    console.log('Could not find bot account token key.');
    return;
}
if (!process.env.EXPOSE_URL) {
    console.log('Could not find exposing url');
    return;
}

//create Bot
const bot = new ViberBot( {
    authToken: process.env.BOT_ACCOUNT_TOKEN,
    name: "Sewing Buddy",
    avatar: "https://images.app.goo.gl/Y8vrphEPzxpXGmvk6"
});


bot.on(BotEvents.SUBSCRIBED, response => {
    response.send(new TextMessage(`Thanks for subscribing ${response.userProfile.name} 🎉 `));
});

bot.onTextMessage(/^hi$|^hello$/i, (message, response) =>
    response.send(new TextMessage(`Hi there ${response.userProfile.name}. I am your ${bot.name}. I give advice to sewing beginners :) \r\nYou can type any of the following numbers to learn about: \r\n1. Needle sizes \r\n2. Stitch Type \r\n3. Sewing Machine Parts \r\n4. Top 2 Sewing Machines`)));


bot.onTextMessage(/^1$/, (message, response) =>
    response.send(new TextMessage(`Type menu to views needle options per fabric 🧵 `)));

const CarouselContentGenerator = require('./carousel_content_generator');

let carouselBuilder = new CarouselContentGenerator('#FFFFFF');
    carouselBuilder.addElement('Strech Fabric', '110/18 ', 'https://www.seamwork.com/media/articles/wp/2015/01/10-activewear-1024x682.jpg','Yellow Needle', 'action body 1', true);
    
    carouselBuilder.addElement('Jeans', '90/14', 'https://thumbs.dreamstime.com/b/jeans-fabric-close-up-pattern-blue-denim-textile-texture-fashion-cloth-copy-space-indigo-material-background-191536511.jpg','Blue Needle', 'action body 2', true);
    
    carouselBuilder.addElement('Leather', '120/19', 'https://media.istockphoto.com/photos/brown-leather-picture-id537284406?k=6&m=537284406&s=612x612&w=0&h=yiebpqXAM8nsQDgG5KcWnOpxDRFrExMenrwR0tOScaw=','Brown Needle', 'action body 2', true);
    
    carouselBuilder.addElement('Jersey', '80/12', 'https://media.istockphoto.com/photos/red-sports-clothing-fabric-football-jersey-texture-close-up-picture-id1179885091?k=6&m=1179885091&s=612x612&w=0&h=2in8Wq0UL8hEtbV4YxPaHpMT6sa8Y60ESLcplsXl5Vs=','Orange Needle', 'action body 2', true);

    
bot.onTextMessage(/^menu$/i,(message, response) => 
    response.send(new RichMedia(carouselBuilder.build())));
    

bot.on(BotEvents.MESSAGE_RECEIVED, (message,response) => {
    if (test(message)===true){
    response.send(new TextMessage(`Sorry, I don't understand everything! I am just a bot 🥺 Say hi/hello to me to start chating and to guide you ✍️ `))}
});


function test(my_str){
    a = my_str.text
    return !a.match(/^menu$|^1$|^2$|^3$|^hi$|^hello$|^4$|^bye$|^goodbye$|^https/i);
}

bot.onTextMessage(/^2$/, (message, response) => {
    response.send(new TextMessage(`The basic stitch options are: \r\n Straight \r\n Zig-zag \r\n\ and Strech stitch`))
    //response.send(new ImageMessage('https://www.brother-usa.com/virdata/CSFAQS/HAD/CP4%20Low-End/UtilityStitchChart.PNG'))
});

bot.onTextMessage(/^3$/, (message, response) => {
    response.send(new ImageMessage('https://upload.wikimedia.org/wikipedia/commons/c/c7/Sewingmachine1.jpg'))
});

const SAMPLE_KEYBOARD = {
	"Type": "keyboard",
	"Revision": 1,
	"Buttons": [
		{
			"Columns": 3,
			"Rows": 2,
			"BgColor": "#e6f5ff",
			"BgMedia": "https://a.scdn.gr/images/sku_main_images/002860/2860265/xlarge_20200720113417_singer_tradition_2250.jpeg",
			"BgMediaType": "picture",
			"Silent":true,
            "ActionType": "open-url",
			"ActionBody": "https://www.skroutz.gr/s/2860265/Singer-Tradition-2250.html",
		},
        {
			"Columns": 3,
			"Rows": 2,
			"BgColor": "#e6f5ff",
			"BgMedia": "https://d.scdn.gr/images/sku_main_images/000350/350072/xlarge_20200731112838_singer_promise_1409.jpeg",
			"BgMediaType": "picture",
			"Silent":true,
            "ActionType": "open-url",
			"ActionBody": "https://www.skroutz.gr/s/350072/Singer-Promise-1409.html",
		}
	]
};

bot.onTextMessage(/^4$/, (message, response) => {
    try {
        response.send(new KeyboardMessage(SAMPLE_KEYBOARD))
    } catch (error) {
        console.log(error)
        
    }
});

bot.onTextMessage(/^bye$|^goodbye$/i, (message, response) =>
    response.send(new TextMessage(`Bye ${response.userProfile.name} 👋 It was nice taking to you`)));


bot.on(BotEvents.UNSUBSCRIBED, response => {
    response.send(new TextMessage(`Sorry to see you leaving ${response.userProfile.name} :()`));
});


const port = process.env.PORT || 80;
app.use("/viber/webhook", bot.middleware());
app.listen(port, () => {
    console.log(`Application running on port: ${port}`);
    bot.setWebhook(`${process.env.EXPOSE_URL}/viber/webhook`).catch(error => {
        console.log('Can not set webhook on following server. Is it running?');
        console.error(error);
        process.exit(1);
    });
});