Viber Bot Creation -- Enneas Assignment Liarou Eleni

## Project Purpose
The CHATBOT called Sewing Buddy is used to help sewing beginners.

## Prerequisites

- Installed node.js 
- Installed npm


## Connection 
To connect we create a secure tunnel from public endpoint to our locally running application.

We use ```npm i -g ngrok ``` to install ngrok 

and then:

``` ngrok http 80 ```

We change the EXPOSE_URL according to the one the connection generates in two places in the index.js file. 


Then we run the index.js file

 ``` node index.js ```

## Access Bot On viber
To access the bot on viber we can scan the QR code given in the image QRcode.JPG from the Viber App on our smartphone. 